import java.security.SecureRandom;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        String resultado = generarClave();
        System.out.println(resultado);
    }

    public static String generarClave() {

        // Caracteres permitidos para la clave
        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";

        // Longitud mínima y máxima de la clave
        int longitudMinima = 8;
        int longitudMaxima = 15;

        // Generador de números aleatorios seguro
        SecureRandom random = new SecureRandom();

        // Determinar la longitud aleatoria de la clave
        int longitudClave = random.nextInt(longitudMaxima - longitudMinima + 1) + longitudMinima;

        // Inicializar un StringBuilder para construir la clave
        StringBuilder claveBuilder = new StringBuilder();

        // Agregar al menos una letra mayúscula
        claveBuilder.append(caracteres.charAt(random.nextInt(26))); // Letra mayúscula aleatoria

        // Agregar al menos una letra minúscula
        claveBuilder.append(caracteres.charAt(random.nextInt(26) + 26)); // Letra minúscula aleatoria

        // Agregar al menos un carácter especial
        claveBuilder.append(caracteres.charAt(random.nextInt(caracteres.length() - 52) + 52)); // Carácter especial aleatorio

        // Generar el resto de la clave
        for (int i = 3; i < longitudClave; i++) {
            claveBuilder.append(caracteres.charAt(random.nextInt(caracteres.length())));
        }

        // Convertir el StringBuilder a una cadena y devolverlo
        return claveBuilder.toString();
    }

    public static void validarCedula() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese su cedula");

        String indentificacionIngresada = sc.nextLine();

        // expresion regular para validar cedula que tenga 10 digitos o 13 digitos
        if (indentificacionIngresada.matches("[0-9]{10}") || indentificacionIngresada.matches("[0-9]{13}")) {

            int codigoProvincia = Integer.parseInt(indentificacionIngresada.substring(0, 2));

            // validar que el codigo de la provincia este entre 1 y 24


            if (codigoProvincia < 0 || codigoProvincia > 24) {
                System.out.println("IDENTIFICACION INVALIDA");
            } else {

                String cedula = indentificacionIngresada.substring(0, 9);
                String digitoVerificador = indentificacionIngresada.substring(9, 10);
                boolean validacion = algoritmoModulo10(cedula, Integer.parseInt(digitoVerificador));

                if (validacion) {
                    String mensajeResultado = indentificacionIngresada.length() == 10 ? "CEDULA DE " : "RUC DE";
                    if (codigoProvincia == 9) {
                        mensajeResultado += " GUAYAQUIL";
                    } else {
                        mensajeResultado += " OTRA PROVINCIA";
                    }
                    System.out.println(mensajeResultado);
                } else {
                    System.out.println("IDENTIFICACION INVALIDA");
                }
            }


        } else {
            System.out.println("IDENTIFICACIÓN INVALIDA");
        }


    }

    public static boolean algoritmoModulo10(String digitosIniciales, int digitoVerificador) {
        Integer[] arrayCoeficientes = new Integer[]{2, 1, 2, 1, 2, 1, 2, 1, 2};

        Integer[] digitosInicialesTMP = new Integer[digitosIniciales.length()];
        int indice = 0;
        for (char valorPosicion : digitosIniciales.toCharArray()) {
            digitosInicialesTMP[indice] = Integer.parseInt(String.valueOf(valorPosicion));
            indice++;
        }

        int total = 0;
        int key = 0;

        for (Integer valorPosicion : digitosInicialesTMP) {
            if (key < arrayCoeficientes.length) {
                valorPosicion = (digitosInicialesTMP[key] * arrayCoeficientes[key]);

                if (valorPosicion >= 10) {
                    char[] valorPosicionSplit = String.valueOf(valorPosicion).toCharArray();
                    valorPosicion = (Integer.parseInt(String.valueOf(valorPosicionSplit[0]))) + (Integer.parseInt(String.valueOf(valorPosicionSplit[1])));

                }
                total = total + valorPosicion;
            }

            key++;
        }
        int residuo = total % 10;
        int resultado;

        if (residuo == 0) {
            resultado = 0;
        } else {
            resultado = 10 - residuo;
        }

        return resultado == digitoVerificador;
    }
}